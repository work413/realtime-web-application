import '../socket.io/socket.io.js'

const template = document.querySelector('#template')
const closeTemplate = document.querySelector('#closed-template')

if (template) {
  const hbsTemplate = window.Handlebars.compile(template.innerHTML)

  const socket = window.io()

  // Handles the creating and updating event.
  socket.on('issue', arg => {
    const issueString = hbsTemplate(arg)
    const div = document.createElement('div')

    const list = document.getElementById('list')

    const allIDs = document.getElementsByName('issue-id')

    // Looks for the issue among the currently displayed issues.
    for (let i = 0; i < allIDs.length; i++) {
      // If the issue is already displayed, replace it with an updated one.
      if (allIDs[i].value === arg.id) {
        const container = allIDs[i].parentNode
        const issueContainer = container.parentNode
        issueContainer.parentNode.innerHTML = issueString
        return
      }
    }

    // Does this if the issue is not among the currently displayed issues.
    div.innerHTML = issueString

    const latestIssue = list.firstChild

    list.insertBefore(div, latestIssue)
  })

  // Handles the close event.
  socket.on('closed', arg => {
    const allIDs = document.getElementsByName('issue-id')

    // Looks for the issue among the currently displayed issues.
    for (let i = 0; i < allIDs.length; i++) {
      // If the issue is already displayed, replace it with an updated one.
      if (allIDs[i].value === arg.id) {
        const container = allIDs[i].parentNode
        const issueContainer = container.parentNode

        const list = document.getElementById('list')
        list.removeChild(issueContainer.parentNode)
      }
    }
  })
}

if (closeTemplate) {
  const hbsTemplate = window.Handlebars.compile(closeTemplate.innerHTML)

  const socket = window.io()

  // Handles the close event when on the "closed issues" page.
  socket.on('closed', arg => {
    const issueString = hbsTemplate(arg)
    const div = document.createElement('div')

    const list = document.getElementById('list')

    div.innerHTML = issueString

    const latestIssue = list.firstChild

    list.insertBefore(div, latestIssue)
  })
}
