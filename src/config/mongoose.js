/**
 * Database connection.
 *
 * @author Martina Andersson
 * @version 1.0.0
 */

import mongoose from 'mongoose'

/**
 * Connects to mongoDB database.
 *
 * @returns {Promise} Does something if connection is made.
 */
export const connectDB = async () => {
  mongoose.connection.on('connected', () => console.log('Mongoose connection is open.'))
  mongoose.connection.on('error', err => console.error(`Mongoose connection error has occurred: ${err}`))
  mongoose.connection.on('disconnected', () => console.log('Mongoose connection is disconnected.'))

  return mongoose.connect(process.env.DB_CONNECTION_STRING, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true
  })
}
