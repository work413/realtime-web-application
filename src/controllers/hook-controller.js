/**
 * Controller.
 *
 * @author Martina Andersson
 * @version 1.0.0
 */

/**
 * Controller class.
 */
export class HookController {
  /**
   * Creates a new body with all the neccessary information.
   *
   * @param {object} req - Request object.
   * @param {object} res - Response object.
   * @param {Function} next - Next function.
   */
  index (req, res, next) {
    req.body = {
      title: req.body.object_attributes.title,
      description: req.body.object_attributes.description,
      id: req.body.object_attributes.iid,
      avatar: req.body.user.avatar_url,
      action: req.body.object_attributes.action,
      done: false
    }

    next()
  }
}
