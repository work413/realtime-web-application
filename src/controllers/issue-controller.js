/**
 * Issue Controller.
 *
 * @author Martina Andersson
 * @version 1.0.0
 */

import { IssueModel } from '../models/issue.js'
import fetch from 'node-fetch'

/**
 * Controller class.
 */
export class IssueController {
  /**
   * Creates a new issue from realtime request from GitLab.
   *
   * @param {object} req - Request object.
   * @param {object} res - Response object.
   * @param {Function} next - Next function.
   */
  async newIssue (req, res, next) {
    try {
      const issue = new IssueModel({
        title: req.body.title,
        description: req.body.description,
        avatar: req.body.avatar,
        thisID: req.body.id,
        action: req.body.action,
        done: req.body.done
      })

      await issue.save()

      // Emits this event if the user closed an issue.
      if (issue.action === 'close') {
        res.io.emit('closed', {
          title: issue.title,
          description: issue.description,
          done: issue.done,
          state: issue.state,
          avatar: issue.avatar,
          id: issue.thisID
        })
      } else {
        // Emits this event for any other issue update.
        res.io.emit('issue', {
          title: issue.title,
          description: issue.description,
          done: issue.done,
          state: issue.state,
          avatar: issue.avatar,
          id: issue.thisID
        })
      }

      if (req.headers['x-gitlab-event']) {
        res.status(200).send('Accepted')
        return
      }

      req.session.flash = { message: 'Issue successfully created' }
      res.redirect('.')
    } catch (error) {
      req.session.flash = { message: 'Failed to create issue' }
      res.redirect('../')
    }
  }

  /**
   * Closes an issue.
   *
   * @param {object} req - Request object.
   * @param {object} res - Response object.
   * @param {Function} next - Next function.
   */
  async close (req, res, next) {
    try {
      // Send a PUT request to close an issue.
      const closeRequest = await fetch('https://gitlab.lnu.se/api/v4/projects/13984/issues/' + req.params.id + '?state_event=close', {
        method: 'PUT',
        headers: {
          'PRIVATE-TOKEN': process.env.PRIVATE_TOKEN,
          'Content-Type': 'application/json'
        }
      })

      if (closeRequest.status === 401) {
        req.session.flash = { message: 'Failed to close issue.' }
        res.redirect('..')
      } else {
        req.session.flash = { message: 'Successfully closed issue.' }
        res.redirect('..')
      }
    } catch (error) {
      req.session.flash = { message: 'Failed to close issue' }
      res.redirect('..')
    }
  }

  /**
   * Renders the create page.
   *
   * @param {object} req - Request object.
   * @param {object} res - Response object.
   * @param {Function} next - Next function.
   */
  renderNew (req, res, next) {
    res.render('issue-pages/new')
  }

  /**
   * Creates a new issue.
   *
   * @param {object} req - Request object.
   * @param {object} res - Response object.
   * @param {Function} next - Next function.
   */
  async create (req, res, next) {
    try {
      // Sends a POST request to create a new issue.
      const createRequest = await fetch('https://gitlab.lnu.se/api/v4/projects/13984/issues?title=' + req.body.issue, {
        method: 'POST',
        headers: {
          'PRIVATE-TOKEN': process.env.PRIVATE_TOKEN,
          'Content-Type': 'application/json'
        }
      })

      if (createRequest.status === 401) {
        req.session.flash = { message: 'Failed to create new issue.' }
        res.redirect('..')
      } else {
        req.session.flash = { message: 'Successfully created new issue.' }
        res.redirect('..')
      }
    } catch (error) {
      req.session.flash = { message: 'Failed to create new issue.' }
      res.redirect('..')
    }
  }
}
