/**
 * Controller.
 *
 * @author Martina Andersson
 * @version 1.0.0
 */

import fetch from 'node-fetch'

/**
 * Controller class.
 */
export class Controller {
  /**
   * Authorizes the user and calls the next function after.
   *
   * @param {object} req - Request object.
   * @param {object} res - Response object.
   * @param {Function} next - Next function.
   */
  authorize (req, res, next) {
    if (req.headers['x-gitlab-token'] !== process.env.HOOK_SECRET) {
      res.status(403).send('incorrect')
      return
    }
    next()
  }

  /**
   * Renders a home page with all open issues displayed.
   *
   * @param {object} req - Request object.
   * @param {object} res - Response object.
   * @param {Function} next - Next function.
   */
  async index (req, res, next) {
    try {
      // Fetches all issues.
      const allIssues = await fetch('https://gitlab.lnu.se/api/v4/projects/13984/issues', {
        method: 'GET',
        headers: {
          'PRIVATE-TOKEN': process.env.PRIVATE_TOKEN,
          'Content-Type': 'application/json'
        }
      }).then(data => {
        return data.json()
      }).then(response => {
        return response
      })

      const viewData = {
        all: (allIssues)
          .map(obj => ({
            id: obj.iid,
            title: obj.title,
            description: obj.description,
            avatar: obj.author.avatar_url,
            open: obj.state === 'opened'
          }))
      }

      res.render('issue-pages/index', { viewData })
    } catch (error) {
      next(error)
    }
  }

  /**
   * Renders a page with all the closed issues displayed.
   *
   * @param {object} req - Request object.
   * @param {object} res - Response object.
   * @param {Function} next - Next function.
   */
  async closed (req, res, next) {
    try {
      const allIssues = await fetch('https://gitlab.lnu.se/api/v4/projects/13984/issues', {
        method: 'GET',
        headers: {
          'PRIVATE-TOKEN': process.env.PRIVATE_TOKEN,
          'Content-Type': 'application/json'
        }
      }).then(data => {
        return data.json()
      }).then(response => {
        return response
      })

      const viewData = {
        all: (allIssues)
          .map(obj => ({
            id: obj.iid,
            title: obj.title,
            description: obj.description,
            avatar: obj.author.avatar_url,
            closed: obj.state === 'closed'
          }))
      }

      res.render('issue-pages/closed', { viewData })
    } catch (error) {
      req.session.flash = { message: 'Failed to render the page' }
      res.redirect('.')
    }
  }
}
