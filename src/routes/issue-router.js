/**
 * Issue router.
 *
 * @author Martina Andersson
 * @version 1.0.0
 */

import express from 'express'
import { IssueController } from '../controllers/issue-controller.js'
import { Controller } from '../controllers/controller.js'

export const router = express.Router()
const issueController = new IssueController()
const controller = new Controller()

router.get('/', controller.index)
router.get('/closed', controller.closed)
router.get('/new', issueController.renderNew)

router.post('/create', issueController.create)

router.get('/:id/close', issueController.close)
