/**
 * Router.
 *
 * @author Martina Andersson
 * @version 1.0.0
 */

import express from 'express'
import createError from 'http-errors'
import { router as issueRouter } from './issue-router.js'
import { router as hookRouter } from './hook-router.js'

export const router = express.Router()

router.use('/', issueRouter)

router.use('/webhook', hookRouter)

router.use('*', (req, res, next) => next(createError(404)))
