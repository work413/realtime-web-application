/**
 * Hook router.
 *
 * @author Martina Andersson
 * @version 1.0.0
 */

import express from 'express'
import { Controller } from '../controllers/controller.js'
import { IssueController } from '../controllers/issue-controller.js'
import { HookController } from '../controllers/hook-controller.js'

export const router = express.Router()

const controller = new Controller()
const issueController = new IssueController()
const hookController = new HookController()

router.post('/issue', controller.authorize, hookController.index, issueController.newIssue)
