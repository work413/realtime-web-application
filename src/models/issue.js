/**
 * Mongoose schema
 *
 * @author Martina Andersson
 * @version 1.0.0
 */

import mongoose from 'mongoose'

// Schema for issue object.
const issue = new mongoose.Schema({
  type: Object,
  title: {
    type: String
  },
  description: {
    type: String
  },
  avatar: {
    type: String
  },
  thisID: {
    type: String
  },
  action: {
    type: String
  },
  done: {
    type: Boolean,
    default: false
  }
})

// Create model from schema.
export const IssueModel = mongoose.model('Issue', issue)
